module.exports = function() {
  return new Promise((resolve, reject) => {
      // Selector for PLAYER 
      const S_PLAYER = '.statsTableContainer > tr';
      // Selector for PLAYER NAME
      const S_PLAYER_NAME = '.playerName > strong';
      // Selector for PLAYER NACIONALITY
      const S_PLAYER_NACIONALITY = '.playerCountry';
      // Selector for PLAYER RANK
      const S_PLAYER_RANK = '.rank > strong';
      // Selector for PLAYER GOALS
      const S_PLAYER_GOALS = '.mainStat';

      // Pagination parameters
      const T_PAGE_BODY = document.querySelector('.statsRow + div');
      const T_PAGE_SIZE = Number(T_PAGE_BODY.getAttribute('data-page-size'));
      const T_PAGE_ENTRIES = Number(T_PAGE_BODY.getAttribute('data-num-entries'));
      const T_PAGINATION_NEXT_BUTTON = document.querySelector('.paginationNextContainer');
      const T_PAGINATION_PAUSE = 2000;
      const T_PAGE_LAST = Math.ceil(T_PAGE_ENTRIES / T_PAGE_SIZE);
      let T_PAGE_CURRENT = 1;
      
      const _log = console.info,
          _warn = console.warn,
          _error = console.error,
          _time = console.time,
          _timeEnd = console.timeEnd;

      _time("Scrape");


      // Global Set to store all entries
      let premierLeaguePlayers = new Set(); // Eliminates dupes
      let nacionalitys = new Object();

      // Accepts a parent DOM element and extracts the data
      function scrapeSinglePlayer(player) {
          try {
              const name = player.querySelector(S_PLAYER_NAME).innerText.trim(),
                  nacio = player.querySelector(S_PLAYER_NACIONALITY).innerText.trim(),
                  rank = player.querySelector(S_PLAYER_RANK).innerText.trim(),
                  goals = player.querySelector(S_PLAYER_GOALS).innerText.trim();
                  nacionalitys[nacio] = (nacionalitys[nacio] + 1) || 1;
              premierLeaguePlayers.add({
                    rank,
                    name,
                    nacio,
                    goals
                });
          } catch (e) {
              _error("Error capturing individual player", e.message);
          }
      }

      // Get all players in the table page
      function scrapePlayers() {
        _log('Page ', T_PAGE_CURRENT);
          const players = document.querySelectorAll(S_PLAYER);

          if (players.length > 0) {
              _log("Scraping page ... found " + players.length + " players");
              Array.from(players).forEach(scrapeSinglePlayer);
          } else {
              _warn("Scraping page ... found no players");
          }
      }

      // Recursive loop that ends when there are no more players
      function loop() {
        _log("Looping..." + premierLeaguePlayers.size + " entries added");
        scrapePlayers()
        if (T_PAGE_CURRENT <= T_PAGE_LAST) {
          try {
              T_PAGINATION_NEXT_BUTTON.click();
              T_PAGE_CURRENT++;
              setTimeout(loop, T_PAGINATION_PAUSE);
              _log("Loading next page... ");
          } catch (e) {
              reject(e.message);
          }
        } else {
            _timeEnd("Scrape");
            resolve({Nacionalidades: nacionalitys, Jogadores: Array.from(premierLeaguePlayers)});
        }
    }
    loop();
  });
}