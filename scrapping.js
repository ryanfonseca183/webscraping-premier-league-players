const puppeteer = require('puppeteer')
const script = require('./script');
const { writeFileSync } = require("fs");

function save(raw) {
    writeFileSync('results.txt', JSON.stringify(raw, null, "\t"));
}

const URL = 'https://www.premierleague.com/stats/top/players/goals?se=-1&cl=-1&iso=-1&po=-1?se=-1';

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  page.on('console', msg => console.log(msg.text()));
  await page.goto(URL);
  await page.waitForSelector('.paginationNextContainer');
  const players = await page.evaluate(script);
  save(players);
  await browser.close();
})();